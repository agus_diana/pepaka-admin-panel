<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 04/04/2021
 * Time: 14.33
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visits extends Model
{
    use SoftDeletes;

    protected $table = 'visits';
    protected $fillable = ['user_id','to_user_id','description','notes','date_time'];

    public function galleries() {
        return $this->hasMany('App\VisitGalleries','visit_id','id');
    }

    public function toUser() {
        $user = $this->hasOne('App\User','id','to_user_id');
        return $user->select('name','id');
    }

    public function user() {
        return $this->hasOne('App\User','id','user_id');
    }
}
