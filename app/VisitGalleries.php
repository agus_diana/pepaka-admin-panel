<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 04/04/2021
 * Time: 14.35
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisitGalleries extends Model
{
    use SoftDeletes;
    protected $table = 'visit_galleries';
    protected $fillable = ['visit_id','filename'];
}
