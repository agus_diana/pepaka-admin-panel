<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 15/03/2021
 * Time: 01.21
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class UserEvents extends Model
{
    protected $table = 'user_events';
    protected $fillable = ['user_id','event_id','name','comments','custom_attribute','date','start_time','end_time','reference_id'];

    public function user() {
        return $this->hasOne('App\User','id','user_id');
    }
}
