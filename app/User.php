<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'typeReg', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token', // 'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public function generateToken()
//    {
//        $this->_token = str_random(60);
//        $this->save();
//
//        return $this->_token;
//    }
    public function role() {
        return $this->hasOne('App\Roles','id','role');
    }

    public function fieldAssistance() {
        $fa = $this->hasOne('App\User','id','activated_by');
        return $fa->where('role',5);
    }

    public function visitsFA() {
        $vis = $this->hasMany('App\Visits','user_id','id');
        $vis->with('toUser');
        return $vis->orderBy('date_time','desc');
    }

    public function farmers() {
        $farmers = $this->hasMany('App\User','activated_by','id');
        $farmers->whereNotNull('activated_at');
        return $farmers->where('role',4);
    }
}
