<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 24/03/2021
 * Time: 16.18
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    public function statusDesc(){
        return $this->hasOne('App\OrderStatuses','id','status');
    }
}
