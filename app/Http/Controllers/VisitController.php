<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 25/04/2021
 * Time: 09.26
 */

namespace App\Http\Controllers;

use App\Visits;
use Illuminate\Http\Request;
class VisitController extends Controller
{
    public function index(Request $request){
        $visitQuery = Visits::query();
        $visitQuery->with('user','toUser');
        $visitQuery->orderBy('created_at','desc');
        $visits = $visitQuery->paginate(10);
        return view('visit',['visits' => $visits]);
    }

    public function showOne(Request $request){
        $id = $request->input('id');
        $visitQuery = Visits::query();
        $visitQuery->with('user','toUser','galleries');
        $visit = $visitQuery->find($id);

        return view('visitDetail',['data' => $visit]);
    }
}
