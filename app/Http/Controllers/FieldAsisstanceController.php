<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 25/04/2021
 * Time: 15.47
 */

namespace App\Http\Controllers;

use App\User;
use App\Visits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class FieldAsisstanceController extends Controller
{
    public function index(Request $request){
        $userQuery = User::query();
        $userQuery->withCount('farmers');
        $userQuery->with('visitsFA','farmers');
        $userQuery->where('role', 5);
        $users = $userQuery->paginate(10);
        foreach ($users as $key => $value) {
            $users[$key]->filename = 'noimage.png';
            $filename = DB::table('image_uploads')->find($value->imageid);
            if ($filename) $users[$key]->filename = $filename->filename;
        }
        return view('fieldAsisstance',['data' => $users]);
    }

    public function showOne(Request $request){
        $id = $request->input('id');
        $userQuery = User::query();
        $userQuery->with('visitsFA','farmers');
        $user = $userQuery->find($id);

        $user->address_desc = '';
        if ($user->address) {
            $addrArr = explode('|',$user->address);
            $user->address_desc = $addrArr[3] . ', ' . $addrArr[2] . ', ' . $addrArr[1] . ', ' . $addrArr[0];
        }

        foreach ($user->farmers as $key => $value) {
            $user->farmers[$key]->filename = 'noimage.png';
            $filename = DB::table('image_uploads')->find($value->imageid);
            if ($filename) $user->farmers[$key]->filename = $filename->filename;

            // get history visit
            $visits = Visits::query();
            $visits->where('user_id', $id);
            $visits->where('to_user_id', $value->id);
            $visits->orderBy('date_time','desc');
            $user->farmers[$key]->visit_history = $visits->get();
        }

        $user->filename = 'noimage.png';
        $filename = DB::table('image_uploads')->find($user->imageid);
        if ($filename) $user->filename = $filename->filename;

        return view('fieldAsisstanceDetail',['data' => $user]);
    }
}
