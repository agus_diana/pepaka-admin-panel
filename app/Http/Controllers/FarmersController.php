<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 12/04/2021
 * Time: 10.17
 */

namespace App\Http\Controllers;


use App\ImageUpload;
use App\User;
use App\UserEvents;
use App\Visits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FarmersController extends Controller
{
    public function index(Request $request) {
        $userQuery = User::query();
        $userQuery->with('fieldAssistance');
        $userQuery->where('role', 4);
        $userQuery->orderBy('active','ASC');
        $userQuery->orderBy('activated_at');
        $users = $userQuery->paginate(8);
        foreach ($users as $key => $value) {
            $users[$key]->filename = 'noimage.png';
            $filename = DB::table('image_uploads')->find($value->imageid);
            if ($filename) $users[$key]->filename = $filename->filename;
        }

        return view('farmers',['data' => $users]);
    }

    public function showOne(Request $request) {
        $id = $request->input('id');
        $userQuery = User::query();
        $userQuery->with('fieldAssistance');
        $farmer = $userQuery->find($id);
        $farmer->address_desc = '';
        if ($farmer->address) {
            $addrArr = explode('|',$farmer->address);
            $farmer->address_desc = $addrArr[3] . ', ' . $addrArr[2] . ', ' . $addrArr[1] . ', ' . $addrArr[0];
        }

        $ladangs = DB::table('user_ladang')->where('user_id', $id)->get();
        foreach ($ladangs as $key => $value) {
            $images = DB::table('ladang_images')->where('ladang_id', $value->id)->pluck('image_id');
            $filenames = ImageUpload::whereIn('id', $images)->pluck('filename');
            $ladangs[$key]->image = $filenames;
        }
        $farmer->ladang = $ladangs;

        $farmer->status_desc = 'Belum aktif';
        $farmer->note = '';
        if ($farmer->active) {
            $farmer->note = 'Aktif sejak ' . date('d-M-Y H:i:s', strtotime($farmer->approved_at));
            $farmer->status_desc = 'Aktif';
        } else {
            if ($farmer->activate_scheduled) {
                if (!$farmer->approved) {
                    if ($farmer->activated_at) {
                        $farmer->status_desc = 'Menunggu approval';
                        $farmer->note = 'Sudah disurvey pada ' .date('d-M-Y H:i:s', strtotime($farmer->activated_at));
                    } else {
                        $farmer->status_desc = 'Menunggu survey';
                        $farmer->note = '';
                    }
                } else {
                    $event = UserEvents::query();
                    $event->with('user');
                    $event->where('reference_id', $farmer->id);
                    $event->where('event_id', 1);
                    $schedule = $event->first();
                    $farmer->note = 'Jadwal survey ' . date('d-M-Y', strtotime($schedule->date)) . ' ' . date('H:i', strtotime($schedule->start_time)) . ' s/d ' . date('H:i', strtotime($schedule->end_time));
                }
            } else {
                $farmer->status_desc = 'Menunggu jadwal survey';
            }
        }

        $filename = DB::table('image_uploads')->where("id", $farmer->imageid)->get()->first();
        if ($filename != null)
            $farmer->filename = $filename->filename;
        else
            $farmer->filename = "noimage.png";

        $visiteds = Visits::with('galleries')->where('to_user_id', $id)->orderBy('date_time', 'desc')->get();
        foreach ($visiteds as $key => $visited) {
            $seconds_ago = (time() - strtotime($visited->date_time));
            if ($seconds_ago >= 31536000) {
                $visiteds[$key]->time_desc = intval($seconds_ago / 31536000) . " tahun yang lalu";
            } elseif ($seconds_ago >= 2419200) {
                $visiteds[$key]->time_desc = intval($seconds_ago / 2419200) . " bulan yang lalu";
            } elseif ($seconds_ago >= 86400) {
                $visiteds[$key]->time_desc = intval($seconds_ago / 86400) . " hari yang lalu";
            } elseif ($seconds_ago >= 3600) {
                $visiteds[$key]->time_desc = intval($seconds_ago / 3600) . " jam yang lalu";
            } elseif ($seconds_ago >= 60) {
                $visiteds[$key]->time_desc = intval($seconds_ago / 60) . " menit yang lalu";
            } else {
                $visiteds[$key]->time_desc = "beberapa menit yang lalu";
            }
        }
        $farmer->visited = $visiteds;

        return view('farmersProfile',['data' => $farmer]);
    }

    public function approve(Request $request)
    {
        $farmerId = $request->input('id');

        $user = Auth()->user();
        $userId = $user->id;

        $farmer = User::find($farmerId);
        $farmer->approved_by = $userId;
        $farmer->approved_at = date('Y-m-d H:i:s');
        $farmer->approved = 1;
        $farmer->active = 1;
        $farmer->save();

        $messagingCont = new MessagingController();
        $request->request->add([
            'user' => $farmerId,
            'title' => 'Aktivasi Akun Anda',
            'text' => 'Selamat akun anda telah berhasil diaktivasi.',
            'imageid' => 100
        ]);
        $messagingCont->sendNotify($request, 'api');

        return redirect('farmer?id=' . $farmerId)->with(['success' => 'Petani ' . $farmer->name . ' berhasil di aktivasi !.']);
    }

}
