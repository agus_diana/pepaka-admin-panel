<?php
namespace App\Http\Controllers\API\owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB;
use App\Logging;
use Auth;
use App\UserInfo;

class FoodsController extends Controller
{
    public function load(Request $request)
    {
        $imageId = null;
        if (UserInfo::getUserRoleId() == 2) { // manager
            $foods = DB::table('manager_rest')->where('user', '=', Auth::user()->id)->join("foods", 'foods.restaurant', '=', 'manager_rest.restaurant')->get();
            $restaurants = DB::table('manager_rest')->where('user', '=', Auth::user()->id)->join("restaurants", 'restaurants.id', '=', 'manager_rest.restaurant')->get();
        }else{
            if ($request->input('published') && $request->input('published') == 1) {
                $foods = DB::table('foods')->where('published',1)->get();
                $imageCollection = collect($foods);
                $imageId = $imageCollection->pluck('imageid')->values();
            } else {
                $foods = DB::table('foods')->get();
                $imageCollection = collect($foods);
                $imageId = $imageCollection->pluck('imageid')->values();
            }
            $restaurants = DB::table('restaurants')->select('id', 'name', 'published')->get();
        }

        if ($imageId) {
            $images = DB::table('image_uploads')->select('id', 'filename')->whereIn('id',$imageId)->get();
        } else {
            $images = DB::table('image_uploads')->select('id', 'filename')->get();
        }

        $extrasGroup = DB::table('extrasgroup')->get();
        $nutritionGroup = DB::table('nutritiongroup')->get();
        $temp = DB::table('settings')->where('param', '=', "default_currencyCode")->get()->first()->value;
        $symbolDigits = DB::table('currencies')->where('code', '=', $temp)->get()->first()->digits;
        $response = [
            'error' => '0',
            'id' => "",
            'images' => $images,
            'foods' => $foods,
            'restaurants' => $restaurants,
            'extrasGroup' => $extrasGroup,
            'nutritionGroup' => $nutritionGroup,
            'numberOfDigits' => $symbolDigits,
        ];
        return response()->json($response, 200);
    }

    public function foodsRet($id)
    {
        $images = DB::table('image_uploads')->select('id', 'filename')->get();
        $foods = DB::table('foods')->get();
        $restaurants = DB::table('restaurants')->select('id', 'name', 'published')->get();
        $extrasGroup = DB::table('extrasgroup')->select('id', 'name', 'restaurant')->get();
        $nutritionGroup = DB::table('nutritiongroup')->get();
        $response = [
            'error' => '0',
            'id' => $id,
            'images' => $images,
            'foods' => $foods,
            'restaurants' => $restaurants,
            'extrasGroup' => $extrasGroup,
            'nutritionGroup' => $nutritionGroup,
        ];
        return response()->json($response, 200);
    }

    public function foodSave(Request $request)
    {
        $edit = $request->input('edit') ?: "0";
        $editId = $request->input('editId') ?: "0";

        $values = array(
            'name' => $request->input('name'),
            'imageid' => $request->input('imageid') ?: 0,
            'price' => $request->input('price'),
            'discountprice' => 0,
            'desc' => $request->input('desc') ?: "",
            'restaurant' => $request->input('restaurant') ?: 0,
            'category' => $request->input('category') ?: 0,
            'ingredients' => $request->input('ingredients') ?: "",
            'unit' => "",
            'packageCount' => 0,
            'weight' => 0,
            'canDelivery' => 1,
            'published' => $request->input('published'),
            'stars' => 5,
            'extras' => $request->input('extras') ?: 0,
            'nutritions' => $request->input('nutritions') ?: 0,
            'updated_at' => new \DateTime());

        $id = $editId;
        if ($edit == '1')
            DB::table('foods')->where('id',$editId)->update($values);
        else{
            $values['created_at'] = new \DateTime();
            DB::table('foods')->insert($values);
            $id = DB::getPdo()->lastInsertId();
        }

        return FoodsController::foodsRet($id);
    }

    public function foodDelete(Request $request)
    {
        $id = $request->input('id');
        DB::table('foods')->where('id',$id)->delete();
        return FoodsController::foodsRet("");
    }


}
