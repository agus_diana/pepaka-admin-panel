<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 04/04/2021
 * Time: 14.38
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use App\Visits;
use Illuminate\Http\Request;

class VisitController extends Controller
{
    public function index() {

        $user_id = Auth()->user()->id;
        $visits = Visits::query();
        $visits->where('user_id', $user_id);
        $visits->with('toUser');
        $visits->orderBy('date_time','desc');
        $data = $visits->get();

        $response = [
            'error' => '0',
            'data' => $data,
        ];
        return response()->json($response, 200);
    }

    public function show($id) {
        $data = Visits::with('galleries','toUser')->find($id);

        $response = [
            'error' => '0',
            'data' => $data,
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request) {
        $user_id = Auth()->user()->id;
        $to_user_id = $request->input('to_user_id');
        $description = $request->input('description');
        $notes = $request->input('notes');
        $date_time = $request->input('date_time');

        $created = Visits::create([
            'user_id' => $user_id,
            'to_user_id' => $to_user_id,
            'description' => $description,
            'notes' => $notes,
            'date_time' => $date_time,
        ]);

        //create user event calendar
        $toUser = User::find($to_user_id);
        $request->request->add([
            'event_id' => '2',
            'name' => 'Kunjungan Rutin',
            'comments' => 'Kunjungan Rutin#' . $toUser->name,
            'date' => date('Y-m-d', strtotime($date_time)),
            'start_time' => date('H:i:s', strtotime($date_time)),
            'reference_id' => $created->id
        ]);
        $userEventCont = new UserEventController();
        $userEventCont->createEvent($request);

        $response = [
            'error' => '0',
            'data' => $created,
        ];
        return response()->json($response, 200);
    }

    public function update(Request $request, $id) {
        $visits = Visits::find($id);
        $visits->to_user_id = $request->input('to_user_id');
        $visits->description = $request->input('description');
        $visits->notes = $request->input('notes');
        $visits->date_time = $request->input('date_time');
        $visits->save();
        $data = $visits->refresh();

        $response = [
            'error' => '0',
            'data' => $data,
        ];
        return response()->json($response, 200);
    }


    public function destroy($id) {
        $visits = Visits::find($id);
        $visits->delete();

        $response = [
            'error' => '0',
            'data' => 1,
        ];
        return response()->json($response, 200);
    }

    public function toUser($to_user_id){
        $user_id = Auth()->user()->id;
        $visitsQuery = Visits::query();
        $visitsQuery->where('user_id', $user_id);
        $visitsQuery->where('to_user_id', $to_user_id);
        $visitsQuery->orderBy('date_time', 'ASC');
        $visits = $visitsQuery->get();

        $response = [
            'error' => '0',
            'data' => $visits,
        ];
        return response()->json($response, 200);
    }
}
