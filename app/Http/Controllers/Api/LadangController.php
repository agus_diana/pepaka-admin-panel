<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 24/03/2021
 * Time: 17.45
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LadangController extends Controller
{
    public function show($id) {
        $ladang = DB::table('user_ladang')->find($id);
        if ($ladang) {
            $ladangImage = DB::table('ladang_images');
            $ladangImage->join('image_uploads','ladang_images.image_id','=','image_uploads.id');
            $ladangImage->where('ladang_images.ladang_id', $id);
            $ladangImage->select('image_uploads.*');
            $ladang->images = $ladangImage->get();
        }

        $response = [
            'error' => '0',
            'ladang' => $ladang,
        ];
        return response()->json($response, 200);
    }
}
