<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\MessagingController;
use App\UserEvents;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\ImageUpload;
use Illuminate\Support\Facades\Hash;
use App\Logging;
use Illuminate\Support\Str;
use Kreait\Firebase\JWT\Error\IdTokenVerificationFailed;
use Kreait\Firebase\JWT\IdTokenVerifier;
use Image;
class AuthController extends Controller
{
    public function phoneAuth(Request $request)
    {
        $phone = $request->input('phone');
        if ($phone) {

        } else {
            $idTokenString = $request->input('token_id');
            $verifier = IdTokenVerifier::createWithProjectId(env('FIREBASE_PROJECT_ID'));

            try {
                $token = $verifier->verifyIdToken($idTokenString);
            } catch (IdTokenVerificationFailed $e) {
                return response(['error' => 1, 'message' => 'Invalid Token Credential']);
            }

            $tokenDecoded = $token->payload();
            $phone = $tokenDecoded['phone_number'];
        }

        $user = DB::table('users')->where('phone', "=", $phone)->get()->first();
        $request->request->add([
            'typeReg' => 'phone',
            'phone' => $phone
        ]);

        if (!$user) {
            return AuthController::register($request);
        }

        return AuthController::login($request);
    }

    public function register(Request $request)
    {
        $typeReg = ($request->input('typeReg') != null ? $request->input('typeReg') : "email");
        $validatedData['typeReg'] = $typeReg;

        $validatedData['role'] = 4;
        if ($request->input('role') != null)
            $validatedData['role'] = $request->input('role');

        if ($typeReg == 'phone') {
            $validatedData['phone'] = $request->input('phone');
        } else {
            $email = $request->input('email');
            $user = DB::table('users')->where('email', "=", $email)->get()->first();
            if ($user != null && $typeReg == "email")
                return response(['error' => '3']);  // This email is busy
            if ($user != null)
                return AuthController::login($request);

            $validatedData['email'] = $request->input('email');
            $validatedData['name'] = $request->input('name');
            $validatedData['password'] = bcrypt($request->password);
        }

        if ($typeReg == "email")
            Logging::logapi("User Create. email=" . $email);
        else
            Logging::logapi("User Create. type=" . $typeReg);

        $userOrg = User::create($validatedData);

        $accessToken = $userOrg->createToken('authToken')->accessToken;
        $user = DB::table('users')->where('id', "=", $userOrg->id)->get()->first();

        $photoUrl = $request->input('photoUrl');
        if ($photoUrl != "") {
            $image = file_get_contents($photoUrl);
            $uuid = Str::uuid()->toString();
            $avatar = "$uuid.jpg";
            file_put_contents(public_path("images/$avatar"), $image);
            //
            $imageUpload = new ImageUpload();
            $imageUpload->filename = $avatar;
            $imageUpload->save();

            $id = $user->id;
            DB::table('users')->where('id', "=", $id)
                ->update(array('imageid' => $imageUpload->id, 'updated_at' => new \DateTime()));
            $user->avatar = $avatar;
        }else{
            // user avatar
            $petani = DB::table('image_uploads')->get();
            $user->avatar = "noimage.png";
            foreach ($petani as &$value2)
                if ($user->imageid == $value2->id)
                    $user->avatar = $value2->filename;
        }

        //get all user Field Assistance to send notification about registered client
        $fcbUsers = DB::table('users')
            ->where('role',5)
            ->whereNotNull('fcbToken')
            ->pluck('fcbToken');

        if ($fcbUsers) {
            $messagingCont = new MessagingController();
            $request->request->add([
                'registration_ids' => $fcbUsers,
                'title' => 'Pendaftaran Member Baru',
                'text' => 'Segera atur jadwal survey.',
                'imageid' => 100
            ]);
            $messagingCont->sendNotify($request, 'api');
        }

        return response(['error' => '0', 'user' => $user, 'access_token' => $accessToken]);
    }


    public function login(Request $request)
    {
        if ($request->input('typeReg') == 'phone') {
            $phone = $request->input('phone');
            $user = DB::table('users')->where('phone', "=", $phone)->get()->first();
            if (!$user) {
                return response(['error' => 1, 'message' => 'Invalid Credentials']);
            }
            auth()->loginUsingId($user->id);
        } else {
            $loginData = $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);

            if (!auth()->attempt($loginData)) {
                return response(['error' => 1, 'message' => 'Invalid Credentials']);
            }
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        $user = auth()->user();

        //
        // for driver app
        //
        $driver = $request->input('driver') ?: "";
        if ($driver == 'true'){
            if ($user->role != 3)
                return response(['error' => 2, 'message' => 'No driver']);
            Logging::logapi("Login Driver");
        }else
            Logging::logapi("Login");

        //
        // for owner app
        //
        $owner = $request->input('owner') ?: "";
        if ($owner == 'true'){
            if ($user->role != 1 && $user->role != 2)
                return response(['error' => 2, 'message' => 'Not owner']);
            Logging::logapi("Login Owner");
        }

        // user avatar
        $petani = DB::table('image_uploads')->get();
        $user['avatar'] = "noimage.png";
        foreach ($petani as &$value2)
            if ($user->imageid == $value2->id)
                $user['avatar'] = $value2->filename;

        $id = auth()->user()->id;
        $notify2 = DB::table('notifications')->where('user', '=', "$id")->where('delete', '=', "0")->where('read', '=', "0")->get();
        $notify = count($notify2);
        return response(['error' => 0, 'user' => $user, 'access_token' => $accessToken, 'notify' => $notify]);

    }

    public function uploadAvatar(Request $request)
    {
        Logging::logapi("Update Avatar");

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();
        $filename = md5(date('YmdHis') . trim($imageName));
        $img = Image::make($image->path());
        $thumbnailPath = public_path('thumbnails');
        $imagePath = public_path('images');
        $img->orientate();
        $img->resize(150,150, function ($constraint) {
            $constraint->aspectRatio();
        })->save($thumbnailPath . '/' . $filename.'.'.$fileExtension);
        $image->move($imagePath, $filename.'.'.$fileExtension);

        $imageUpload = new ImageUpload();
        $imageUpload->filename = $filename.'.'.$fileExtension;
        $imageUpload->save();

        $id = auth('api')->user()->id;
        DB::table('users')->where('id', "=", $id)
            ->update(array('imageid' => $imageUpload->id, 'updated_at' => new \DateTime()));

        return response()->json(['ret'=>true, 'avatar' => $filename.'.'.$fileExtension]);
    }

    public function forgot(Request $request)
    {
        $error = '0';
        $email = $request->input('email');
        Logging::logapi("Forgor password. Email=" . $email);

        $user = DB::table('users')->where('email', "=", $email)->get()->first();
        if ($user == null)
            return response(['error' => '5000']);   // user not found

        $recipient = $email;

        $sender = 'sender@valeraletun.ru';
        $subject = "Password recovery";

        $pass = AuthController::rand_passwd();
        $message = '<html><body>';
        $message .= '<h1>Hello!</h1>';
        $message .= '<p>You received this email because you requested a password recovery in the FoodDelivery mobile app.</p>';
        $message .= '<p>Your new password: ' . $pass . ' </p>';
        $message .= '</body></html>';

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        // Create email headers
        $headers .= 'From: '.$sender."\r\n".
            'Reply-To: '.$sender."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        if (mail($recipient, $subject, $message, $headers))
        {
            $encpass = bcrypt($pass);
            $values = array('password' => $encpass,
                'updated_at' => new \DateTime());
            DB::table('users')
                ->where('email', $email)
                ->update($values);
        }
        else
            $error = '5001'; // cant send email

        return response(['error' => $error]);
    }

    function rand_passwd( $length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ) {
        return substr( str_shuffle( $chars ), 0, $length );
    }

    public function fcbToken(Request $request){
        $fcbToken = $request->input('fcbToken');
        $id = auth('api')->user()->id;
        $values = array('fcbToken' => $fcbToken,
            'updated_at' => new \DateTime());
        DB::table('users')
            ->where('id', $id)
            ->update($values);
        return response(['error' => '0']);
    }

    public function changePassword(Request $request){

        Logging::logapi("Change Password");

        $oldPassword = $request->input('oldPassword') ?: "";
        $newPassword = $request->input('newPassword') ?: "";
        $id = auth('api')->user()->id;
        $user = DB::table('users')->where('id', $id)->get()->first();
        $pass = auth('api')->user()->password;
        if (!Hash::check($oldPassword, $pass))
            return response(['error' => '1']);
        if (strlen($newPassword) < 6)
            return response(['error' => '2']);
        $values = array('password' => bcrypt($newPassword),
            'updated_at' => new \DateTime());
        auth('api')->user()->password = bcrypt($newPassword);
        auth('api')->user()->save();
        DB::table('users')
            ->where('id', $id)
            ->update($values);
        return response(['error' => '0']);
    }

    public function changeProfile(Request $request){
        Logging::logapi("Change Profile");
        $name = $request->input('name') ?: "";
        $email = $request->input('email') ?: "";
        $phone = $request->input('phone') ?: "";
        $address = $request->input('address') ?: ""; // menambahakan alamat
        $id = auth('api')->user()->id;
        $values = array('name' => $name, 'email' => $email, 'phone' => $phone, 'address' => $address,
            'updated_at' => new \DateTime());
        auth('api')->user()->address = $address;
        if ($phone != '') {
            auth('api')->user()->phone = $phone;
        }
        if ($email != '') {
            auth('api')->user()->email = $email;
        }
        auth('api')->user()->name = $name;
        auth('api')->user()->save();
        return response(['error' => '0']);
    }

    public function profile()
    {
        $user = Auth()->user();
        $userId = $user->id;
        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        $notify2 = DB::table('notifications')->where('user', '=', "$userId")->where('delete', '=', "0")->where('read', '=', "0")->get();
        $notify = count($notify2);

        // user avatar
        $petani = DB::table('image_uploads')->where('id', $user->imageid)->first();
        $user['avatar'] = "noimage.png";
        if ($petani) {
            $user['avatar'] = $petani->filename;
        }

        // activate schedule
        $activateSchedule = UserEvents::where('event_id',1)->where('reference_id',$userId)->first();
        $user['activate_schedule'] = $activateSchedule;

        return response(['error' => 0, 'user' => $user, 'access_token' => $accessToken, 'notify' => $notify]);
    }


}
