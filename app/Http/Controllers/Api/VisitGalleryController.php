<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 04/04/2021
 * Time: 19.34
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\VisitGalleries;
use Illuminate\Http\Request;
use Image;
class VisitGalleryController extends Controller
{
    public function store(Request $request) {
        $image = $request->file('images');
        $visitId = $request->input('visit_id');

        $imageName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();
        $filename = md5(date('YmdHis') . trim($imageName));
        $img = Image::make($image->path());
        $thumbnailPath = public_path('thumbnails');
        $imagePath = public_path('images');
        $img->orientate();
        $img->resize(150,150, function ($constraint) {
            $constraint->aspectRatio();
        })->save($thumbnailPath . '/' . $filename.'.'.$fileExtension);
        $image->move($imagePath, $filename.'.'.$fileExtension);

        $created = VisitGalleries::create([
            'visit_id' => $visitId,
            'filename' => $filename.'.'.$fileExtension,
        ]);

        $response = [
            'error' => '0',
            'data' => $created,
        ];
        return response()->json($response, 200);
    }

    public function destroy($id) {
        $image = VisitGalleries::find($id);
        $image->delete();

        $response = [
            'error' => '0',
            'data' => 1,
        ];
        return response()->json($response, 200);
    }
}
