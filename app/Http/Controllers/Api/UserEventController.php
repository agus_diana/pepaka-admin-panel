<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 15/03/2021
 * Time: 00.56
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Controllers\MessagingController;
use App\UserEvents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserEventController extends Controller
{
    public function index() {
        $userId = Auth()->user()->id;
        $data = UserEvents::where('user_id',$userId)
            ->orderBy('date')
            ->orderBy('start_time')
            ->get();

        $response = [
            'error' => 0,
            'data'=> $data,
        ];

        return response()->json($response, 200);
    }

    public function getEventByDate($date) {
        $userId = Auth()->user()->id;
        $data = UserEvents::where('user_id',$userId)
            ->where('date', $date)
            ->orderBy('date')
            ->orderBy('start_time')
            ->get();

        $response = [
            'error' => 0,
            'data'=> $data,
        ];

        return response()->json($response, 200);
    }

    public function createEvent(Request $request) {
        $userId = Auth()->user()->id;
        $eventId = $request->input('event_id');
        $name = $request->input('name');
        $comments = $request->input('comments');
        $date = $request->input('date');
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $referenceId = $request->input('reference_id');

        $created = UserEvents::create([
                'user_id' => $userId,
                'event_id' => $eventId,
                'name' => $name,
                'comments' => $comments,
                'date' => $date,
                'start_time' => $startTime,
                'end_time' => $endTime,
                'reference_id' => $referenceId
            ]);

        if ($created) {
            if ($eventId == 1) {
                DB::table('users')->where('id',$referenceId)->update([
                    'activate_scheduled' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                $messagingCont = new MessagingController();
                $request->request->add([
                    'user' => $referenceId,
                    'title' => 'Jadwal Survey Lokasi Anda ' . date('d M', strtotime($date)),
                    'text' => 'Petugas kami akan datang ke lokasi anda, pastikan nomor telepon anda dapat dihubungi.',
                    'imageid' => 100
                ]);
                $messagingCont->sendNotify($request, 'api');
            }
        }

        $response = [
            'error' => 0,
            'data'=> $created,
        ];

        return response()->json($response, 200);
    }

    public function deleteEvent(Request $request, $id){
        $find = UserEvents::find($id);
        $referenceId = $find->reference_id;

        $deleted = UserEvents::where('id',$id)
            ->delete();

        if ($deleted) {
            DB::table('users')->where('id',$referenceId)->update([
                'activate_scheduled' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            if ($find->event_id == 1) {
                $messagingCont = new MessagingController();
                $request->request->add([
                    'user' => $referenceId,
                    'title' => 'Survey ke lokasi anda akan dijadwalkan ulang',
                    'text' => 'Kami akan memberitahukan anda jika jadwal sudah tersedia.',
                    'imageid' => 100
                ]);
                $messagingCont->sendNotify($request, 'api');
            }
        }

        $response = [
            'error' => 0,
            'data'=> 1,
        ];

        return response()->json($response, 200);
    }

    public function updateEvent(Request $request, $id){
        $eventId = $request->input('event_id');
        $name = $request->input('name');
        $comments = $request->input('comments');
        $date = $request->input('date');
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $referenceId = $request->input('reference_id');

        $updated = UserEvents::where('id',$id)
            ->update([
                'event_id' => $eventId,
                'name' => $name,
                'comments' => $comments,
                'date' => $date,
                'start_time' => $startTime,
                'end_time' => $endTime,
                'reference_id' => $referenceId
            ]);

        if ($updated) {
//            if ($updated->event_id == 1) {
                DB::table('users')->where('id',$referenceId)->update([
                    'activate_scheduled' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
//            }
        }

        $response = [
            'error' => 0,
            'data'=> $updated,
        ];

        return response()->json($response, 200);
    }

    public function getReferenceListByEvent($id) {
        $userId = Auth()->user()->id;
        $data = UserEvents::where('user_id',$userId)
            ->where('event_id', $id)
            ->orderBy('date', 'ASC')
            ->orderBy('start_time', 'ASC')
            ->get();

        $result = [];
        if ($id == 1) {
            foreach ($data as $key => $value) {
                $reference = DB::table('users')
                    ->where('id', $value->reference_id)
                    ->whereNull('activated_by')
                    ->first();
                if ($reference) {
                    $data[$key]->reference_data = $reference;
                    $result[] = $data[$key];
                }
            }
        }

        $response = [
            'error' => 0,
            'data'=> $result,
        ];

        return response()->json($response, 200);
    }
}
