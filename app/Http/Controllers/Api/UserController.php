<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 26/02/2021
 * Time: 09.30
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Controllers\MessagingController;
use App\ImageUpload;
use App\UserEvents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;

class UserController extends Controller
{
    public function index(Request $request){
        $user = Auth()->user();
        $userId = $user->id;
        $activateScheduled = $request->input('activateScheduled');

        if ($activateScheduled && $activateScheduled == 1) {
            $users = DB::table('users')
                ->select('id','name','phone','imageid')
                ->where("active", 0)
                ->where('role',4)
                ->where('id','!=',$userId)
                ->where('activate_scheduled', 1)
                ->orderBy('created_at','DESC')
                ->get();
        } elseif ($activateScheduled == 0) {
            $users = DB::table('users')
                ->select('id','name','phone','imageid')
                ->where("active", 0)
                ->where('role',4)
                ->where('id','!=',$userId)
                ->where('activate_scheduled', 0)
                ->orderBy('created_at','DESC')
                ->get();
        } else {
            $users = DB::table('users')
                ->select('id','name','phone','imageid')
                ->where("active", 0)
                ->where('role',4)
                ->where('id','!=',$userId)
                ->orderBy('created_at','DESC')
                ->get();
        }

        $user = [];
        foreach ($users as $key => $value){
            $user[] = $value;
            $avatar = DB::table('image_uploads')->where('id', $value->imageid)->first();
            $user[$key]->avatar = "noimage.png";
            if ($avatar) {
                $user[$key]->avatar = $avatar->filename;
            }
        }

        $response = [
            'error' => '0',
            'user' => $user,
        ];
        return response()->json($response, 200);
    }

    public function getOne($id){
        $user = DB::table('users')
            ->select('id','name','email','email_verified_at','created_at','updated_at','address','phone','active','typeReg','imageid','activated_at','activated_by')
            ->find($id);
        if ($user) {
            $avatar = DB::table('image_uploads')->where('id', $user->imageid)->first();
            $user->avatar = "noimage.png";
            if ($avatar) {
                $user->avatar = $avatar->filename;
            }
        }

        $surveyEvent = UserEvents::where('event_id',1)
            ->where('reference_id',$id)
            ->first();

        $user->survey_event = $surveyEvent;

        $response = [
            'error' => '0',
            'user' => $user,
        ];
        return response()->json($response, 200);
    }

    public function activate(Request $request, $id){
        $user = Auth()->user();
        $userId = $user->id;

        DB::table('users')
            ->where('id',$id)
            ->whereNull('activated_at')
            ->update([
                'activated_by' => $userId,
                'activated_at' => date('Y-m-d H:i:s')
            ]);

        $messagingCont = new MessagingController();
        $request->request->add([
            'user' => $id,
            'title' => 'Aktivasi Akun Anda',
            'text' => 'Selamat akun anda telah berhasil diaktivasi.',
            'imageid' => 100
        ]);
        $messagingCont->sendNotify($request, 'api');

        return response(['error' => '0']);
    }

    public function countNotActive(Request $request){
        $user = Auth()->user();
        $userId = $user->id;
        $activateScheduled = $request->input('activateScheduled');

        if ($activateScheduled && $activateScheduled == 1) {
            $users = DB::table('users')
                ->where("active", 0)
                ->where('role',4)
                ->where('id','!=',$userId)
                ->where('activate_scheduled', 1)
                ->count();
        } elseif ($activateScheduled == 0) {
            $users = DB::table('users')
                ->where("active", 0)
                ->where('role',4)
                ->where('id','!=',$userId)
                ->where('activate_scheduled', 0)
                ->count();
        } else {
            $users = DB::table('users')
                ->where("active", 0)
                ->where('role',4)
                ->where('id','!=',$userId)
                ->count();
        }

        $response = [
            'error' => '0',
            'user' => $users,
        ];
        return response()->json($response, 200);
    }

    public function ladang($userId){
        $ladangs = DB::table('user_ladang')
            ->where('user_id', $userId)
            ->get();

        $ladangArr = [];
        foreach ($ladangs as $key => $value) {
            $images = DB::table('ladang_images')
                ->select('image_id','filename','image_uploads.created_at','image_uploads.updated_at')
                ->join('image_uploads','ladang_images.image_id','=','image_uploads.id')
                ->where('ladang_id', $value->id)
                ->get();
            $ladangArr[] = $value;
            $ladangArr[$key]->images = $images;
        }

        $response = [
            'error' => '0',
            'ladang' => $ladangArr,
        ];
        return response()->json($response, 200);
    }

    public function createLadang(Request $request, $id){
        $data = [
            'user_id' => $id,
            'description' => $request->input('description'),
            'luas' => $request->input('luas'),
            'address' => $request->input('address'),
            'lat' => $request->input('lat'),
            'long' => $request->input('long'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $userLadangId = DB::table('user_ladang')->insertGetId($data);

        $images = $request->file('images');
        foreach ($images as $image) {
            $imageName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $filename = md5(date('YmdHis') . trim($imageName));
            $img = Image::make($image->path());
            $thumbnailPath = public_path('thumbnails');
            $imagePath = public_path('images');
            $img->resize(150,150, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbnailPath . '/' . $filename.'.'.$fileExtension);
            $image->move($imagePath, $filename.'.'.$fileExtension);
            $imageUpload = new ImageUpload();
            $imageUpload->filename = $filename.'.'.$fileExtension;
            $imageUpload->save();

            $imageId = $imageUpload->id;

            $createImage = [
                'ladang_id' => $userLadangId,
                'image_id' => $imageId,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            DB::table('ladang_images')->insert($createImage);
        }

        $response = [
            'error' => "0",
            'data'=> $userLadangId,
        ];
        return response()->json($response, 200);
    }

    public function updateLadang(Request $request, $id) {
        $data = [
            'description' => $request->input('description'),
            'luas' => $request->input('luas'),
            'address' => $request->input('address'),
            'lat' => $request->input('lat'),
            'long' => $request->input('long'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        DB::table('user_ladang')->where('id', $id)->update($data);
        DB::table('ladang_images')->where('ladang_id', $id)->delete();

        $images = $request->file('images');

        if ($images) {
            foreach ($images as $image) {
                $imageName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $filename = md5(date('YmdHis') . trim($imageName));
                $img = Image::make($image->path());
                $thumbnailPath = public_path('thumbnails');
                $imagePath = public_path('images');
                $img->resize(150,150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbnailPath . '/' . $filename.'.'.$fileExtension);
                $image->move($imagePath, $filename.'.'.$fileExtension);
                $imageUpload = new ImageUpload();
                $imageUpload->filename = $filename.'.'.$fileExtension;
                $imageUpload->save();

                $imageId = $imageUpload->id;

                $createImage = [
                    'ladang_id' => $id,
                    'image_id' => $imageId,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                DB::table('ladang_images')->insert($createImage);
            }
        }

        $response = [
            'error' => "0",
            'data'=> 1,
        ];
        return response()->json($response, 200);
    }

    public function deleteLadang($id) {
        $response = [
            'error' => "0",
            'data'=> 0,
        ];

        if (DB::table('user_ladang')->where('id',$id)->delete()) {
            $response = [
                'error' => "0",
                'data'=> 1,
            ];
        }

        return response()->json($response, 200);
    }

    public function countActive() {

    }
}
