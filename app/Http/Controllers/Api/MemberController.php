<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 24/03/2021
 * Time: 14.42
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    public function myMembers(Request $request) {
        $count = $request->input('count');
        $userId = Auth()->user()->id;
        if ($count) {
            $user = DB::table('users')
                ->where('activated_by', $userId)
                ->count('id');
        } else {
            $members = DB::table('users')
                ->where('activated_by', $userId)
                ->get();

            $user = [];
            foreach ($members as $key => $value) {
                $user[] = $value;
                $user[$key]->avatar = "noimage.png";
                if ($value->imageid) {
                    $avatar = DB::table('image_uploads')->where('id', $value->imageid)->first();
                    if ($avatar) {
                        $user[$key]->avatar = $avatar->filename;
                    }
                }
            }
        }

        $response = [
            'error' => '0',
            'user' => $user,
        ];
        return response()->json($response, 200);
    }

    public function profile($id) {
        $user = DB::table('users')->find($id);
        $user->avatar = "noimage.png";
        if ($user->imageid) {
            $avatar = DB::table('image_uploads')->where('id', $user->imageid)->first();
            $user->avatar = $avatar->filename;
        }

        $ladang = DB::table('user_ladang')->where('user_id',$user->id)->get();
        $user->ladang = $ladang;

        $orders = Orders::query();
        $orders->with('statusDesc');
        $orders->where('user', $user->id);
        $orders->orderBy('created_at','DESC');

        $user->orders = $orders->get();

        $response = [
            'error' => '0',
            'user' => $user,
        ];
        return response()->json($response, 200);
    }

}
