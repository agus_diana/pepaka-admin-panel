<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB;
use App\Logging;

class NotifyController extends Controller
{
    public function get(Request $request)
    {
        Logging::logapi("Notifications->Get");

        $petani = DB::table('image_uploads')->get();
        $id = auth('api')->user()->id;
        $notify = DB::table('notifications')->where('user', '=', "$id")->where('delete', '=', "0")->orderBy('updated_at', 'desc')->get();
        foreach ($notify as &$value) {
            foreach ($petani as &$value2)
                if ($value->image == $value2->id)
                    $value->image = $value2->filename;
        }

        $values = array('read' => 1);
        DB::table('notifications')->where('user', '=', "$id")->update($values);

        $response = [
            'error' => '0',
            'notify' => $notify,
        ];
        return response()->json($response, 200);
    }

    public function delete(Request $request)
    {
        $userid = auth('api')->user()->id;
        $id = $request->input('id');

        Logging::logapi("Notifications->Delete: id=".$id);

        $values = array(
            'delete' => 1,
            'updated_at' => new \DateTime());

        DB::table('notifications')
            ->where('id',$id)->where('user',$userid)
            ->update($values);

        $response = [
            'error' => '0',
        ];
        return response()->json($response, 200);
    }
}
