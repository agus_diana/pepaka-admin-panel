<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 07/05/2021
 * Time: 08.28
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\UserImageUploads;
use Illuminate\Http\Request;
use Image;

class ImageUserUploadsController extends Controller
{
    public function store(Request $request){
        $userId = auth('api')->user()->id;
        $description = $request->input('description');

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();
        $filename = md5(date('YmdHis') . trim($imageName));
        $img = Image::make($image->path());
        $thumbnailPath = public_path('thumbnails');
        $imagePath = public_path('images');
        $img->orientate();
        $img->resize(150,150, function ($constraint) {
            $constraint->aspectRatio();
        })->save($thumbnailPath . '/' . $filename.'.'.$fileExtension);
        $image->move($imagePath, $filename.'.'.$fileExtension);

        $fileId = UserImageUploads::create([
            'user_id' => $userId,
            'filename' => $filename.'.'.$fileExtension,
            'description' => $description,
        ]);

        return response()->json(['error' => 0, 'data' => $fileId]);
    }

    public function update(Request $request, $id) {
        $description = $request->input('description');

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();
        $filename = md5(date('YmdHis') . trim($imageName));
        $img = Image::make($image->path());
        $thumbnailPath = public_path('thumbnails');
        $imagePath = public_path('images');
        $img->orientate();
        $img->resize(150,150, function ($constraint) {
            $constraint->aspectRatio();
        })->save($thumbnailPath . '/' . $filename.'.'.$fileExtension);
        $image->move($imagePath, $filename.'.'.$fileExtension);

        $image = UserImageUploads::find($id);
        $image->filename = $filename.'.'.$fileExtension;
        $image->description = $description ?? null;
        $image->save();

        $updated = $image->refresh();

        return response()->json(['error' => 0, 'data' => $updated]);
    }

    public function getOneByUser(Request $request){
        $userId = auth('api')->user()->id;
        $description = $request->input('description');

        $file = UserImageUploads::query();
        $file->where('user_id', $userId);
        $file->where('description', $description);
        $fileImage = $file->first();

        return response()->json(['error' => 0, 'data' => $fileImage]);
    }
}
