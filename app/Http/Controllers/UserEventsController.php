<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 12/04/2021
 * Time: 06.18
 */

namespace App\Http\Controllers;

use App\User;
use App\UserEvents;
use App\Visits;
use Illuminate\Http\Request;
class UserEventsController extends Controller
{
    public function index(Request $request) {
        if ($request->ajax()) {
           $query = UserEvents::query();
           $query->whereDate('date', '>=', $request->start);
           $query->whereDate('date','<=', $request->end);
           $query->with('user');
           $events = $query->get();

           $data = [];
           foreach ($events as $key => $value) {
               $data[] = [
                   'id' => $value->id,
                   'start' => $value->date . ' ' . $value->start_time,
                   'end' => $value->date . ' ' . $value->end_time,
                   'user' => $value->user,
                   'event_id' => $value->event_id,
               ];

               if ($value->event_id == 1) {
                   $reference = User::find($value->reference_id);
                   $data[$key]['title'] = $value->user->name . ' | ' . $value->name . '|' . $value->comments . '|' . $reference->name;
               } else {
                   $reference = Visits::with('toUser')->find($value->reference_id);
                   $data[$key]['title'] = $value->user->name . ' | ' . $value->name . '|' . $value->comments . '|' . $reference->toUser->name;
               }
               $data[$key]['reference'] = $reference;
           }


            return response()->json($data);
        }
        return view('calendars');
    }
}
