<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 07/05/2021
 * Time: 08.29
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserImageUploads extends Model
{
    use SoftDeletes;
    protected $table = 'user_image_uploads';
    protected $fillable = ['user_id','filename','description'];
}
