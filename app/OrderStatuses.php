<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 24/03/2021
 * Time: 16.19
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class OrderStatuses extends Model
{
    protected $table = 'orderstatuses';
}
