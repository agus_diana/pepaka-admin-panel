<?php

return [

    'eng' => [
        'id' => '1',
        'name' => 'English',
        'name2' => 'English',
        'file' => 'langEng',
        'dir' => 'ltr',
    ],
    'id' => [
        'id' => '10',
        'name' => 'Indonesia',
        'name2' => 'Bahasa',
        'file' => 'langInd',
        'dir' => 'ltr',
    ],
];
