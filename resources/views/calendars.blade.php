@extends('bsb.app')
@inject('lang', 'App\Lang')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">Kalender</h3>
            </div>
        </div>
    </div>
    <div class="body">
        <div id='calendar'></div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Nama Petugas : <span id="modalNamaPetugas"></span></p>
                        <p>Petani : <span id="modalNamaPetani"></span></p>
                        <p>Waktu : <span id="modalTime"></span></p>
                        <p>Comments : <span id="modalComments"></span> </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function () {
            moment().locale('id').format('LLLL');

            var SITEURL = "{{ url('/') }}";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var calendar = $('#calendar').fullCalendar({
                events: SITEURL + "/calendars",
                displayEventTime: false,
                editable: true,
                eventRender: function (event, element, view) {
                    event.allDay === 'true' ? event.allDay = true : event.allDay = false;
                    console.log(event);
                    if (event.event_id === 1) {
                        element.css('background-color', '#377f3c');
                    } else {
                        element.css('background-color', '#2d0eff');
                    }
                },
                selectable: true,
                selectHelper: true,
                eventClick: function (event) {
                    var startTime = moment(event.start).format('HH:mm');
                    var endTime = moment(event.end).format('HH:mm');
                    var titleArr = event.title.split('|');
                    $('#modalNamaPetugas').html("<a href='fieldAssistance?id=" + event.user.id + "'>" + titleArr[0] + "</a>");

                    if (event.event_id === 1) {
                        $('#modalTitle').html("<a href='farmer?id=" + event.reference.id + "'>" + titleArr[2] + "</a>");
                        $('#modalNamaPetani').html("<a href='farmer?id=" + event.reference.id + "'>" + titleArr[3] + "</a>");
                    } else {
                        $('#modalNamaPetani').html("<a href='farmer?id=" + event.reference.to_user.id + "'>" + titleArr[3] + "</a>");
                        $('#modalTitle').html("<a href='visit?id=" + event.reference.id + "'>" + titleArr[2] + "</a>");
                    }
                    $('#modalComments').html(titleArr[1]);
                    $('#modalTime').text(startTime + ' s/d ' + endTime);
                    $('#myModal').modal()
                    console.log(event);
                }
            });
        });
    </script>
@endsection
