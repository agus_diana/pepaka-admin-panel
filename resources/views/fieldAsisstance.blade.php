@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">List Field Assistance</h3>
                <hr />
            </div>
        </div>
        <div class="row clearfix js-sweetalert">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>No</th> {{--Id--}}
                            <th>Nama</th>
                            <th>Gambar</th>
                            <th>HP</th>
                            <th>Petani</th>
                            <th>Terakhir Kunjungan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr>
                                <td>{{ $data->firstItem() + $key}}</td>
                                <td>{{ $value->name ?? '-no name-' }}</td>
                                <td>
                                <div style="background-image: url('thumbnails/{{ $value->filename }}'); width: 50px; height: 50px; background-size: cover; background-position: top center; border-radius: 50%;">
                                </div>
                                </td>
                                <td>{{ $value->phone }}</td>
                                <td>{{ $value->farmers_count }}</td>
                                <td>{{ count($value->visitsFA) > 0 ? date('d-M-Y', strtotime($value->visitsFA->first()->date_time)) : '' }}</td>
                                <td><a href="fieldAssistance?id={{ $value->id }}" type="button" class="btn btn-default waves-effect">
                                        <img src="img/iconview.png" width="25px">
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
