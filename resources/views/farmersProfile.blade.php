@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">Profil Petani</h3>
                <hr>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-2">
                                <img class="mx-auto d-block" src="/thumbnails/{{ $data->filename }}" height="200px" width="200px" />
                            </div>
                            <div class="col-md-10">
                                @include('elements.form.text', array('label' => 'Nama', 'readonly' => 'readonly', 'text' => '', 'value' => $data->name, 'id' => "name", 'request' => "false", 'maxlength' => "255"))  {{-- Name - Insert Name --}}
                                @include('elements.form.text', array('label' => 'No Handphone','readonly' => 'readonly', 'text' => '', 'value' => $data->phone, 'id' => "phone", 'request' => "false", 'maxlength' => "255"))  {{-- Name - Insert Name --}}
                                @include('elements.form.text', array('label' => 'Alamat', 'readonly' => 'readonly', 'textarea' => true, 'text' => '', 'value' => $data->address_desc, 'id' => "address", 'request' => "false", 'maxlength' => "255"))  {{-- Name - Insert Name --}}
                                @if ($data->fieldAssistance)
                                    @include('elements.form.text', array('label' => 'Nama FA', 'readonly' => 'readonly', 'text' => '', 'value' => $data->fieldAssistance->name, 'id' => "fa", 'request' => "false", 'maxlength' => "255"))
                                @endif
                                <div class="col-md-12 " style="margin-bottom: 0px">
                                    <div class="col-md-4 form-control-label" style="margin-bottom: 0px">
                                        <label><h4>Status</h4>
                                        </label>
                                    </div>
                                    <div class="col-md-8" style="margin-bottom: 0px; margin-top: 7px; vertical-align: middle">
                                        <div class="form-group form-group-lg form-float " style="margin-bottom: 0px">
                                            <div class="form-line">
                                                <h5>{{ $data->status_desc }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if ($data->note)
                                    <div class="col-md-12 " style="margin-bottom: 0px">
                                        <div class="col-md-4 form-control-label" style="margin-bottom: 0px">
                                            <label><h4>Catatan</h4>
                                            </label>
                                        </div>
                                        <div class="col-md-8" style="margin-bottom: 0px; margin-top: 7px; vertical-align: middle">
                                            <div class="form-group form-group-lg form-float " style="margin-bottom: 0px">
                                                <div class="form-line">
                                                    <h5>{{ $data->note }}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4>Ladang</h4>
        @foreach($data->ladang as $item)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $item->description }}</h3>
                </div>
                <div class="panel-body">
                    <p>Luas lahan {{ $item->luas }}m<sup>2</sup></p>
                    <p>Alamat <a href="https://maps.google.com/?q={{ $item->lat }},{{ $item->long }}" target="_blank">{{ $item->address }}</a></p>
                    <div class="row">
                        @foreach ($item->image as $image)
                            <div class="col-md-2" style="padding-right: 0">
                                <a href="#" class="pop">
                                    <img src="/thumbnails/{{ $image }}" style="object-fit: cover; width: 100%; height: 100px; border: 1px solid #ddd; border-radius: 4px;padding: 5px;" />
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
        <p>&nbsp;</p>
        <h4>Riwayat Kunjungan</h4>
        <div class="list-group">
            @foreach ( $data->visited as $value)
                <span class="list-group-item">
                    <small class="pull-right">{{ $value->time_desc }}</small>
                    <h4 class="list-group-item-heading">{{ $value->description }}</h4>
                    <p class="list-group-item-text">{{ $value->notes }}</p>
                    <p class="list-group-item-text"><i>{{ date('d-M-Y H:i', strtotime($value->date_time)) }}</i></p>
                    <div class="row">
                        @foreach ($value->galleries as $image)
                            <div class="col-md-1" style="padding-right: 0">
                                <a href="#" class="pop">
                                    <img src="/thumbnails/{{ $image->filename }}" style="object-fit: cover; width: 100%; height: 60px; border: 1px solid #ddd; border-radius: 4px;padding: 1px;" />
                                </a>
                            </div>
                        @endforeach
                    </div>
                </span>
            @endforeach
        </div>
        <hr />
        @if ($data->approved == 0 and $data->activated_at)
            <p>
                <a href="farmerApprove?id={{ $data->id }}" class="btn-lg btn-primary"><img style="color: white" src="img/iconsend.png" width="25px">Approve</a>
            </p>
        @endif
    </div>

    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <img src="" class="imagepreview" style="width: 100%;" >
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('.pop').on('click', function() {
                const thumbUrl = $(this).find('img').attr('src');
                const filename = thumbUrl.substring(thumbUrl.lastIndexOf('/') + 1);
                const orgUrl = '/images/' + filename;
                $('.imagepreview').attr('src', orgUrl);
                $('#imagemodal').modal('show');
            });
            @if ($message = Session::get('success'))
                showNotification("bg-green", "{{ $message }}", "bottom", "center", "", "");
            @endif
        });
    </script>
@endsection
