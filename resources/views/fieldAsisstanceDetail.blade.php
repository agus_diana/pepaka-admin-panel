@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">Profil Field Assistance</h3>
                <hr>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-2">
                                <img class="mx-auto d-block" src="/thumbnails/{{ $data->filename }}" height="150px" width="150px" />
                            </div>
                            <div class="col-md-10">
                                @include('elements.form.text', array('label' => 'Nama', 'readonly' => 'readonly', 'text' => '', 'value' => $data->name, 'id' => "name", 'request' => "false", 'maxlength' => "255"))  {{-- Name - Insert Name --}}
                                @include('elements.form.text', array('label' => 'No Handphone','readonly' => 'readonly', 'text' => '', 'value' => $data->phone, 'id' => "phone", 'request' => "false", 'maxlength' => "255"))  {{-- Name - Insert Name --}}
                                @include('elements.form.text', array('label' => 'Alamat', 'readonly' => 'readonly', 'textarea' => true, 'text' => '', 'value' => $data->address_desc, 'id' => "address", 'request' => "false", 'maxlength' => "255"))  {{-- Name - Insert Name --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#petani" role="tab" data-toggle="tab">
                        <i class="fa fa-user"></i> Petani
                    </a>
                </li>
                <li>
                    <a href="#kunjungan" role="tab" data-toggle="tab">
                        <i class="fa fa-envelope"></i> Riwayat Kunjungan
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade active in" id="petani">
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">List Petani</div>
                        <ul class="list-group">
                            @foreach($data->farmers as $key => $value)
                                <li class="list-group-item">
                                    <div class="row" style="margin-bottom: 0;">
                                        <div class="col-sm-2" style="margin-bottom: 0;">
                                            <span>
                                                <div style="background-image: url('thumbnails/{{ $value->filename }}'); width: 100px; height: 100px; background-size: cover; background-position: top center; border-radius: 50%;">
                                            </div>
                                            </span>
                                        </div>
                                        <div class="col-sm-10" style="margin-bottom: 0;">
                                            <h5><a href="farmer?id={{ $value->id }}" >{{ $value->name ?? '--no name--' }}</a></h5>
                                            <p>{{ $value->phone }}</p>
                                            <p>Status : <b>{{ $value->approved ? 'Aktif' : 'Menunggu Approval' }}</b></p>
                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onclick="loadTableData({{ $value->visit_history }})">Lihat Riwayat Kunjungan</button>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="tab-pane fade" id="kunjungan">
                    <h2>List Riwayat Kunjungan</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>No</th> {{--Id--}}
                                <th>Nama Petani</th>
                                <th>Tanggal</th>
                                <th>Deskripsi</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data->visitsFA as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td><a href="farmer?id={{ $value->toUser->id }}" >{{ $value->toUser->name }}</a></td>
                                    <td>{{ date('d-M-Y H:i', strtotime($value->date_time)) }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td><a href="visit?id={{ $value->id }}" type="button" class="btn btn-default waves-effect">
                                            <img src="img/iconview.png" width="25px">
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Riwayat Kunjungan Petani</h4>
                </div>
                <div class="modal-body">
                    <table id="visit_history" class="table table-borderless table-striped table-earning">
                        <thead>
                        <tr>
                            <th>No</th> {{--Id--}}
                            <th>Tanggal</th>
                            <th>Deskripsi</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="visit_history_body">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        function loadTableData(items) {
            const table = document.getElementById("visit_history_body");
            table.innerHTML = '';
            items.forEach( (item, index) => {
                let actionStr = "<a href='visit?id=" + item.id + "' type='button' class='btn btn-default waves-effect'>";
                actionStr = actionStr + "<img src='img/iconview.png' width='25px'></a>";

                let row = table.insertRow();

                let number = row.insertCell(0);
                let date = row.insertCell(1);
                let name = row.insertCell(2);
                let action = row.insertCell(3);
                number.innerHTML = index +1;
                date.innerHTML = item.date_time;
                name.innerHTML = item.description;
                action.innerHTML = actionStr;
            });
            console.log(items);
        }
    </script>
@endsection
