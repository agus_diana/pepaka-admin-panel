@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">Detail Kunjungan</h3>
                <hr>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12 " style="margin-bottom: 0px">
                <div class="col-md-2 form-control-label" style="margin-bottom: 0px">
                    <label><h4>Tanggal</h4></label>
                </div>
                <div class="col-md-10" style="margin-bottom: 0px">
                    <div class="form-group form-group-lg form-float " style="margin-bottom: 0px">
                        <div class="form-line">
                            <input type="text" readonly class="form-control" placeholder="" value="{{ date('d-M-Y H:i', strtotime($data->date_time)) }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 " style="margin-bottom: 0px">
                <div class="col-md-2 form-control-label" style="margin-bottom: 0px">
                    <label><h4>Nama FA</h4></label>
                </div>
                <div class="col-md-10" style="margin-bottom: 0px">
                    <div class="form-group form-group-lg form-float " style="margin-bottom: 0px">
                        <div class="form-line">
                            <input type="text" readonly class="form-control" placeholder="" value="{{ $data->user->name }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 " style="margin-bottom: 0px">
                <div class="col-md-2 form-control-label" style="margin-bottom: 0px">
                    <label><h4>Nama Petani</h4></label>
                </div>
                <div class="col-md-10" style="margin-bottom: 0px">
                    <div class="form-group form-group-lg form-float " style="margin-bottom: 0px">
                        <div class="form-line">
                            <input type="text" readonly class="form-control" placeholder="" value="{{ $data->toUser->name }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 " style="margin-bottom: 0px">
                <div class="col-md-2 form-control-label" style="margin-bottom: 0px">
                    <label><h4>Deskripsi</h4></label>
                </div>
                <div class="col-md-10" style="margin-bottom: 0px">
                    <div class="form-group form-group-lg form-float " style="margin-bottom: 0px">
                        <div class="form-line">
                            <input type="text" readonly class="form-control" placeholder="" value="{{ $data->description }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 " style="margin-bottom: 0px">
                <div class="col-md-2 form-control-label" style="margin-bottom: 0px">
                    <label><h4>Catatan</h4></label>
                </div>
                <div class="col-md-10" style="margin-bottom: 0px">
                    <div class="form-group form-group-lg form-float " style="margin-bottom: 0px">
                        <div class="form-line">
                            <textarea readonly class="form-control" placeholder="">{{ $data->notes }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Dokumentasi</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    @foreach ($data->galleries as $image)
                        <div class="col-md-2" style="padding-right: 0">
                            <a href="#" class="pop">
                                <img src="/thumbnails/{{ $image->filename }}" style="object-fit: cover; width: 100%; height: 100px; border: 1px solid #ddd; border-radius: 4px;padding: 5px;" />
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <img src="" class="imagepreview" style="width: 100%;" >
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('.pop').on('click', function() {
                const thumbUrl = $(this).find('img').attr('src');
                const filename = thumbUrl.substring(thumbUrl.lastIndexOf('/') + 1);
                const orgUrl = '/images/' + filename;
                $('.imagepreview').attr('src', orgUrl);
                $('#imagemodal').modal('show');
            });
            @if ($message = Session::get('success'))
            showNotification("bg-green", "{{ $message }}", "bottom", "center", "", "");
            @endif
        });
    </script>
@endsection
