@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">List Petani</h3>
            </div>
        </div>
    </div>
    <div class="row clearfix js-sweetalert">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="margin: 10px 10px 10px 10px;">
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>No</th> {{--Id--}}
                                <th>{{$lang->get(69)}}</th>
                                <th>{{$lang->get(70)}}</th>
                                <th>{{$lang->get(152)}}</th>
                                <th>Status</th>
                                <th>Nama FA</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $value)
                                <tr>
                                    <td>{{ $data->firstItem() + $key}}</td>
                                    <td>{{ $value->name ?? '-no name-' }}</td>
                                    <td>
                                        <div style="background-image: url('thumbnails/{{ $value->filename }}'); width: 50px; height: 50px; background-size: cover; background-position: top center; border-radius: 50%;">
                                        </div>
                                    </td>
                                    <td>{{ $value->phone }}</td>
                                    <td>
                                        @if ($value->active == 1)
                                            Aktif
                                        @else
                                            @if ($value->activate_scheduled == 0)
                                                Menunggu jadwal survey
                                            @else
                                                @if (!$value->activated_by)
                                                    Menunggu survey
                                                @else
                                                    @if ($value->approved == 0)
                                                        Menunggu approval
                                                    @endif
                                                @endif
                                            @endif
                                        @endif
                                    </td>
                                    <td>{{ $value->fieldAssistance->name ?? '' }}</td>
                                    <td><a href="farmer?id={{ $value->id }}" type="button" class="btn btn-default waves-effect">
                                            <img src="img/iconview.png" width="25px">
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div align="center">
                            <nav>
                                {!! $data->links() !!}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
