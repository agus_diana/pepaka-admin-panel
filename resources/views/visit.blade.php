@inject('userinfo', 'App\UserInfo')
@inject('lang', 'App\Lang')
@extends('bsb.app')

@section('content')
    <div class="header">
        <div class="row clearfix">
            <div class="col-md-12">
                <h3 class="">List Kegiatan Kunjungan Field Assistance</h3>
            </div>
        </div>
    </div>
    <div class="row clearfix js-sweetalert">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="margin: 10px 10px 10px 10px;">
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>No</th> {{--Id--}}
                                <th>Tanggal</th>
                                <th>Nama FA</th>
                                <th>Nama Petani</th>
                                <th>Deskripsi</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visits as $key => $value)
                                <tr>
                                    <td>{{ $visits->firstItem() + $key}}</td>
                                    <td>{{ date('d-M-Y H:i', strtotime($value->created_at)) }}</td>
                                    <td>{{ $value->user->name ?? '-no name-' }}</td>
                                    {{--<td>--}}
                                        {{--<div style="background-image: url('thumbnails/{{ $value->filename }}'); width: 50px; height: 50px; background-size: cover; background-position: top center; border-radius: 50%;">--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                    <td>{{ $value->toUser->name }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td><a href="visit?id={{ $value->id }}" type="button" class="btn btn-default waves-effect">
                                            <img src="img/iconview.png" width="25px">
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div align="center">
                            <nav>
                                {!! $visits->links() !!}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
